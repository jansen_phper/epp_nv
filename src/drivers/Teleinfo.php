<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://srs.micang.com All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <jansen.shi@qq.com>
// +----------------------------------------------------------------------
namespace jansen\nv\drivers;
use Metaregistrar\EPP\eppChinaName;
use Metaregistrar\EPP\eppException;
use Metaregistrar\EPP\eppPollRequest;
use Metaregistrar\EPP\eppRealName;
use Metaregistrar\EPP\teleinfoEppCheckNameRequest;
use Metaregistrar\EPP\teleinfoEppCreateNameRequest;
use Metaregistrar\EPP\teleinfoEppInfoNameRequest;
use Metaregistrar\EPP\teleinfoEppUpdateNameRequest;
class Teleinfo extends NameValidator{
    /**
     * 泰尔英福使用自签名证书，且对私钥做了加密，所以此处的证书是手动合成后的证书。
     * 手动合成证书的方法是：将CA证书（ca.crt）、证书（newcert_niucha.pem）、私钥（newkey_niucha.pem）这三个文件的内容复制出来合并即可
     */
    public function __construct(bool $logging=true){
        parent::__construct($logging);
        $this->enableConnectBlock();
    }
    /**
     * @inheritDoc
     */
    public function check(string $name){
        $eppRequest = new teleinfoEppCheckNameRequest(new eppChinaName($name));
        $this->request($eppRequest);
        $result = $this->response->getCheckedNames();
        return $result[0]['available']?'available':($result[0]['restricted']?'restricted':'unavailable');
    }
    /**
     * @inheritDoc
     */
    public function inputInfo(string $code, string $password=null){
        $eppRequest = new teleinfoEppInfoNameRequest('input', $code, $password);
        $this->request($eppRequest);
        return $this->response->getInput();
    }
    /**
     * @inheritDoc
     */
    public function signedCode(string $code, string $password = null){
        $eppRequest = new teleinfoEppInfoNameRequest('signedCode', $code, $password);
        $this->request($eppRequest);
        return $this->response->getSignedCode();
    }
    /**
     * @inheritDoc
     */
    public function process(string $id = null){
        $eppRequest = new eppPollRequest(empty($id)?'req':'ack', $id?:null);
        $this->request($eppRequest);
        return $this->response->getNVResult(empty($id)?'req':'ack');
        
    }
    /**
     * @inheritDoc
     */
    public function resetPassword(string $code, string $password=null){
        if (empty($password)){
            $password = $this->generatePassword(12);
        }
        $eppRequest = new teleinfoEppUpdateNameRequest($code, $password);
        $this->request($eppRequest);
        return ['code'=>$code, 'password'=>$password];
    }
    /**
     * @inheritDoc
     */
    public function nameCreate(string $name, string $rnc = null, string $password=null){
        $eppChinaName = new eppChinaName($name, $rnc, $password);
        if (empty($password)){
            $password = $this->generatePassword(12);
            $eppChinaName->setAuthorisationCode($password);
        }
        $eppRequest = new teleinfoEppCreateNameRequest($eppChinaName);
        $this->request($eppRequest);
        $result = $this->response->getResult();
        if ($result['status'] != 'nonCompliant'){
            $result['password'] = $password;
        }
        return $result;
    }
    /**
     * @inheritDoc
     */
    public function realNameCreate(string $role, string $name, string $number, string $proof, array $docs, string $password=null){
        //检查证件格式及大小
        if (empty($docs)){
            throw new eppException("请指定实名证件数据。");
        }
        $docSize = 0;
        foreach($docs as $doc){
            if (!isset($doc['type']) || !isset($doc['content'])){
                throw new eppException("实名证件数据不完整，必须包含type和content元素。");
            }
            if (!in_array($doc['type'], ['pdf', 'jpg'])){
                throw new eppException("证件类型不支持，仅支持pdf和jpg格式的证件。");
            }
            //将图片字符串写入成图片文件，用于计算图片大小（非尺寸）
            $docTempFile = tempnam(sys_get_temp_dir(), 'nv_');
            file_put_contents($docTempFile, base64_decode($doc['content']));
            $docSize += filesize($docTempFile);
            unlink($docTempFile);
        }
        if ($docSize > 2250000){
            throw new eppException('所有证件大小合计后超出限制，请调整至2.2M以内。');
        }
        $eppRealName = new eppRealName($role, $name, $number, $proof, $docs, $password);
        if (empty($password)){
            $password = $this->generatePassword(12);
            $eppRealName->setAuthorisationCode($password);
        }
        $eppRequest = new teleinfoEppCreateNameRequest($eppRealName);
        $this->request($eppRequest);
        $result = $this->response->getResult();
        if ($result['status'] != 'nonCompliant'){
            $result['password'] = $password;
        }
        return $result;
    }
}