# EPP实名认证

## 介绍
Extensible Provisioning Protocol (EPP) China Name Verification Mapping的PHP实现，基于metaregistrar/php-epp-client开源项目的封装，目前支持的VSP为泰尔英福（[Teleinfo.cn](http://www.teleinfo.cn)），未来还将支持CNNIC。

## 安装教程

1.  composer方式安装后，加载composer的autoload.php即可使用
2.  非composer方式安装时，手动引入本项目的autoloader.php即可

```
特别说明：
　　请检查metaregistrar/php-epp-client版本是否大于1.0.10
　　Composer安装命令：composer require jansen/nv
```

## 使用说明
#### 配置参数说明
```
host：泰尔英福认证服务器地址，OTE环境为ssl://infoid.ote.teleinfo.cn，正式环境为ssl://infoid.teleinfo.cn
port：泰尔英福认证服务器端口，默认为700
username：用户名
password：密码
certpath：证书路径，可将CA证书、私钥、证书合并为一个证书文件
certpwd：证书密码
logpath：日志存储路径，支持传入具体文件或目录，此参数有值则自动开启日志
timeout：超时时间，单位：秒
```
#### 调用方法说明

```
require dirname(__DIR__).'/vendor/jansen/autoloader.php';
//获取认证实例
$nv = \jansen\nv\NV::getInstance('teleinfo', $config);
//命名检查
$result = $nv->check('example');
//生成命名核验验证码
$result = $nv->nameCreate('example');
//生成实名核验验证码
$result = $nv->realNameCreate($role, $name, $number, $proof, $docs);
//获取核验提交的数据
$result = $nv->inputInfo('1-03a5xxxx334axxxxa6e0xxxx68a8c0xxxx');
//获取核验验证码
$result = $nv->signedCode('1-03a5xxxx334axxxxa6e0xxxx68a8c0xxxx');
//更新核验密码
$result = $nv->resetPassword('1-03a5xxxx334axxxxa6e0xxxx68a8c0xxxx');
//获取人工核验进度
$result = $nv->process();
```

